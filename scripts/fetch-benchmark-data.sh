#!/bin/sh

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"
TOP_DIR="${SCRIPT_DIR}/.."

BOB_STEAGALL_CPPCON2018_BASE_URL="https://github.com/BobSteagall/CppNow2018/raw/master/FastConversionFromUTF-8/code"

function print_usage ()
{
    echo "Usage: "
    echo "${BASH_SOURCE} [--help] <data...>"
    echo ""
    echo "Options"
    echo "  -h,--help"
    echo "    Prints help"
    echo ""
    echo "Data"
    echo "  all"
    echo "    Fetch all benchmark data"
    echo "  stress"
    echo "    Fetch stress test data"
    echo "  wiki"
    echo "    Fetch Wikipedia data"
}

# download url file
function download ()
{
    local url="$1"
    local file="$2"
    wget -O "${file}" "${url}"
}

# download_all base_url destdir files
function download_all ()
{
    local base_url="$1"; shift
    local destdir="$1"; shift
    for f in "$@"; do
        download "${base_url}/${f}" "${destdir}/${f}"
    done
}

function fetch_stress_test ()
{
    local base_url="${BOB_STEAGALL_CPPCON2018_BASE_URL}/test_data"
    local destdir="${TOP_DIR}/data/stress"
    local f_prefix="stress_test_"
    local f_suffix=".txt"
    for i in $(seq 0 2); do
        download "${base_url}/${f_prefix}${i}${f_suffix}" "${destdir}/${i}"
    done
}

function fetch_wiki ()
{
    local destdir="${TOP_DIR}/data/wiki"
    local titles=("English language"
                  "हिन्दी"
                  "日本語"
                  "한국어"
                  "Língua portuguesa"
                  "Русский язык"
                  "Svenska"
                  "汉语")
    local langs=("en"
                 "hi"
                 "ja"
                 "ko"
                 "pt"
                 "ru"
                 "sv"
                 "zh")
    local revids=(911356848
                  4272223
                  73858576
                  24761626
                  55787306
                  101650582
                  46122102
                  55678979)
    local outfile=("English_language-en"
                   "Hindi_language-hi"
                   "Japanese_language-ja"
                   "Korean_language-ko"
                   "Portuguese_language-pt"
                   "Russian_language-ru"
                   "Swedish_language-sv"
                   "Chinese_language-zh")
    for i in "${!titles[@]}"; do
        python3 "${SCRIPT_DIR}/download-wiki.py" -V -r "${revids[${i}]}" -l "${langs[${i}]}" \
                "${titles[${i}]}" "${destdir}/${outfile[${i}]}"
    done
}

while [ $# -gt 0 ]; do
    case "$1" in
        -h|--help)
            print_usage
            exit 0
            ;;
        "all")
            FETCH_STRESS_TEST=true
            FETCH_WIKI=true
            break
            ;;
        "stress")
            FETCH_STRESS_TEST=true
            ;;
        "wiki")
            FETCH_WIKI=true
            ;;
    esac
    shift
done

if [ "${FETCH_STRESS_TEST}" = true ]; then
    fetch_stress_test
fi

if [ "${FETCH_WIKI}" = true ]; then
    fetch_wiki
fi
