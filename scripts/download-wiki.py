#!/usr/bin/python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

from argparse import ArgumentParser

import mwclient as mw

from html_to_text import html_to_text, simple_tag, DEFAULT_EXTRACT_TAGS

WIKIPEDIA_DOMAIN = 'wikipedia.org'
DEFAULT_PARSER_WRAP_OUTPUT_CLASS = 'mw-parser-output'

WIKIPEDIA_UNWRAP_TAGS = [simple_tag('div', attrs={'class':DEFAULT_PARSER_WRAP_OUTPUT_CLASS})]
WIKIPEDIA_EXTRACT_TAGS = DEFAULT_EXTRACT_TAGS + \
    [simple_tag(name) for name in ['div', 'table']] + \
    [simple_tag('sup', attrs={'class':'reference'})] + \
    [simple_tag('ol', attrs={'class':'references'})] + \
    [simple_tag(True, attrs={'class':'noprint'})]

DEFAULT_PARSE_KWARGS = dict(disablelimitreport=True,
                            disableeditsection=True,
                            disabletoc=True)

def _create_parser():
    parser = ArgumentParser(description="""Queries the Wikipedia API to extract its content""")
    parser.add_argument('title',
                        help="Page title")
    parser.add_argument('file',
                        help="File for output")
    parser.add_argument('-r', '--revision-id',
                        type=int,
                        help="Page revision ID to extract")
    parser.add_argument('-l', '--language',
                        default='en',
                        help="Language in which to extract page data")
    parser.add_argument('-V', '--verbose',
                        action='store_true',
                        default=False,
                        help="Enable verbose output")
    return parser

def parse_args():
    return _create_parser().parse_args()

def _wikipedia_site(language='en'):
    try:
        return mw.Site("%s.%s" % (language, WIKIPEDIA_DOMAIN))
    except:
        raise ValueError("Invalid language specified: %s" % language)

def _page_current_parsed_text(site, title, use_extracts=False, **kwargs):
    if use_extracts:
        response = site.api('query', prop='extracts|revisions', titles=title,
                            explaintext=True, exsectionformat='plain',
                            rvprop='ids', rvlimit=1, **kwargs)
        pageid = list(response['query']['pages'].keys())[0]
        text = response['query']['pages'][pageid]['extract']
        revid = response['query']['pages'][pageid]['revisions'][0]['revid']
        pageid = int(pageid)
    else:
        response = site.api('parse', prop='text', page=title, **kwargs)
        html = response['parse']['text']['*']
        text = html_to_text(html, parser='beautifulsoup',
                            unwrap_tags=WIKIPEDIA_UNWRAP_TAGS,
                            extract_tags=WIKIPEDIA_EXTRACT_TAGS)
        pageid = response['parse']['pageid']
        revid_response = site.api('query', prop='revisions', titles=title, rvprop='ids', rvlimit=1)
        revid = revid_response['query']['pages'][str(pageid)]['revisions'][0]['revid']
    return {'text':text, 'title':title, 'pageid':pageid, 'revid':revid}

def _page_parsed_text_by_revid(site, revid, **kwargs):
    response = site.api('parse', prop='text', oldid=revid, **kwargs)
    html = response['parse']['text']['*']
    text = html_to_text(html, parser='beautifulsoup',
                        unwrap_tags=WIKIPEDIA_UNWRAP_TAGS,
                        extract_tags=WIKIPEDIA_EXTRACT_TAGS)
    title = response['parse']['title']
    pageid = response['parse']['pageid']
    return {'text':text, 'title':title, 'pageid':pageid, 'revid':revid}

def _get_page(site, title, revid=None, use_extracts=False):
    if revid is None:
        if use_extracts:
            kwargs = dict()
        else:
            kwargs = DEFAULT_PARSE_KWARGS
        page = _page_current_parsed_text(site, title, use_extracts=use_extracts, **kwargs)
    else:
        page = _page_parsed_text_by_revid(site, revid, **DEFAULT_PARSE_KWARGS)
        if page['title'] != title:
            raise ValueError("Mismatch between received and requested page title with revision ID %d: '%s' != '%s'" %
                             (revid, page['title'], title))
    return page

def _save_content(fname, content):
    with open(fname, 'wb') as f:
        f.write(content.encode('utf-8'))

def _print_request(args):
    msg_args = (args.title,)
    if args.revision_id is None:
        msg_fmt_revid = ""
    else:
        msg_fmt_revid = "revision ID %s "
        msg_args += (args.revision_id,)
    msg_args += (args.language,)
    msg_fmt = "%s%s%s" % ("Extracting data for page '%s' ",
                          msg_fmt_revid,
                          "in language '%s'")
    print(msg_fmt % msg_args)

def _print_retrieval(page, language):
    print("Retrieved data for page title '%s' (page ID %d) with revision ID %d in language '%s'." %
          (page['title'], page['pageid'], page['revid'], language))

def main():
    args = parse_args()
    if args.verbose:
        _print_request(args)
    site = _wikipedia_site(language=args.language)
    page = _get_page(site, args.title, revid=args.revision_id)
    if args.verbose:
        _print_retrieval(page, args.language)
    _save_content(args.file, page['text'])

if __name__ == '__main__':
    main()
