#!/usr/bin/python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: MIT
# Copyright 2019 Matthew Krupcale <mkrupcale@matthewkrupcale.com>

from argparse import ArgumentParser
from collections import defaultdict

from html.parser import HTMLParser
import re

try:
    from bs4 import BeautifulSoup
    HAVE_BEAUTIFULSOUP = True
except:
    HAVE_BEAUTIFULSOUP = False

class simple_tag(object):
    def __init__(self, name=None, attrs={}):
        self.name = name
        self.attrs = attrs

    def _name_match(self, name):
        return self.name == True or self.name == name

    def _attr_match(self, attr, value):
        match_value = self.attrs.get(attr)
        return (match_value == True or
                match_value == value or
                match_value in value)

    def _attrs_match(self, attrs={}):
        for attr in self.attrs:
            if attr not in attrs or not self._attr_match(attr, attrs[attr]):
                return False
        return True

    def match(self, name, attrs={}):
        # Matches this tag and its attributes against another tag and its attributes
        return self._name_match(name) and self._attrs_match(attrs=attrs)

MEDIA_TAG_NAMES = ['audio', 'img', 'picture', 'svg', 'video']
DEFAULT_EXTRACT_TAG_NAMES = ['form', 'script', 'style'] + MEDIA_TAG_NAMES
DEFAULT_EXTRACT_TAGS = [simple_tag(name) for name in DEFAULT_EXTRACT_TAG_NAMES]

def _html_to_text_beautifulsoup(html, unwrap_tags=[], extract_tags=DEFAULT_EXTRACT_TAGS):
    """
    Uses BeautifulSoup to convert HTML to a plain text representation

    Parameters

    html : string
        HTML input string
    unwrap_tags : list of simple_tag
        Tags to unwrap in the HTML
    extract_tags : list of simple_tag
        Tags to extract from the HTML
    """
    soup = BeautifulSoup(html, features='lxml')
    for unwrap_tag in unwrap_tags:
        for tag in soup(unwrap_tag.name, attrs=unwrap_tag.attrs):
            tag.unwrap()
    for extract_tag in extract_tags:
        for tag in soup(extract_tag.name, attrs=extract_tag.attrs):
            tag.extract()
    text = soup.get_text()
    # strip white space from lines
    lines = [line.strip() for line in text.splitlines()]
    # break multi-headlines into a line each
    chunks = [phrase.strip() for line in lines for phrase in line.split('  ')]
    # rejoin lines, dropping blank ones
    text = '\n'.join(chunk for chunk in chunks if chunk)
    return text

def _attrs_list_of_tuple_to_dict(attrs):
    attrs_dict = dict()
    for attr in attrs:
        attrs_dict[attr[0]] = attr[1]
    return attrs_dict

def _attrs_list_of_tuple_to_string(attrs):
    return ' '.join(["%s=\"%s\"" % (attr[0], attr[1]) for attr in attrs])

def _tag_attrs_string(name, attrs):
    return ("%s %s" % (name, _attrs_list_of_tuple_to_string(attrs))).strip()

def _match_tag_against(name, attrs, tags):
    for tag in tags:
        if tag.match(name, attrs):
            return True
    return False

class HTMLFilter(HTMLParser):
    def __init__(self):
        super().__init__(convert_charrefs=False)

    def reset(self):
        super().reset()
        self._buf = ""

    def handle_startendtag(self, name, attrs):
        self._buf += "<%s />" % _tag_attrs_string(name, attrs)

    def handle_starttag(self, name, attrs):
        self._buf += "<%s>" % _tag_attrs_string(name, attrs)

    def handle_endtag(self, name):
        self._buf += "</%s>" % name

    def handle_charref(self, name):
        self._buf += "&#%s;" % name

    def handle_entityref(self, name):
        self._buf += "&%s;" % name

    def handle_data(self, data):
        self._buf += data

    def handle_comment(self, data):
        self._buf += "<!--%s-->" % data

    def handle_decl(self, decl):
        self._buf += "<!%s>" % decl

    def handle_pi(self, data):
        self._buf += "<?%s>" % data

    def get_data(self):
        return self._buf

class HTMLUnwrapper(HTMLFilter):
    """
    Unwrapper filter which replaces a tag with the contents of the tag
    """
    def __init__(self, filter_tags=[]):
        super().__init__()
        self.filter_tags = filter_tags

    def reset(self):
        super().reset()
        self._filter_tag_level = defaultdict(lambda: -1)
        self._filter_tag_stack = defaultdict(list)

    def handle_startendtag(self, name, attrs):
        if not _match_tag_against(name, _attrs_list_of_tuple_to_dict(attrs), self.filter_tags):
            super().handle_startendtag(name, attrs)

    def handle_starttag(self, name, attrs):
        filter_tag_match = _match_tag_against(name, _attrs_list_of_tuple_to_dict(attrs), self.filter_tags)
        if name in self._filter_tag_level or filter_tag_match:
            self._filter_tag_level[name] += 1
        if filter_tag_match:
            self._filter_tag_stack[name].append(self._filter_tag_level[name])
        else:
            super().handle_starttag(name, attrs)

    def handle_endtag(self, name):
        if name in self._filter_tag_level:
            if self._filter_tag_level[name]:
                if self._filter_tag_level[name] != self._filter_tag_stack[name][-1]:
                    super().handle_endtag(name)
                else:
                    self._filter_tag_stack[name].pop()
                self._filter_tag_level[name] -= 1
            else:
                self._filter_tag_level.pop(name)
                self._filter_tag_stack.pop(name)
        else:
            super().handle_endtag(name)

class HTMLExtractor(HTMLFilter):
    """
    Extractor filter which removes a tag and its contents
    """
    def __init__(self, filter_tags=[]):
        super().__init__()
        self.filter_tags = filter_tags

    def reset(self):
        super().reset()
        self._filter_tag_level = defaultdict(lambda: -1)
        self._filter = False

    def handle_startendtag(self, name, attrs):
        if not _match_tag_against(name, _attrs_list_of_tuple_to_dict(attrs), self.filter_tags):
            super().handle_startendtag(name, attrs)

    def handle_starttag(self, name, attrs):
        filter_tag_match = _match_tag_against(name, _attrs_list_of_tuple_to_dict(attrs), self.filter_tags)
        if name in self._filter_tag_level or filter_tag_match:
            self._filter_tag_level[name] += 1
        if not self._filter:
            if filter_tag_match:
                self._filter = True
            else:
                super().handle_starttag(name, attrs)

    def handle_endtag(self, name):
        if name in self._filter_tag_level:
            if self._filter_tag_level[name]:
                self._filter_tag_level[name] -= 1
            else:
                self._filter_tag_level.pop(name)
                if not self._filter_tag_level:
                    self._filter = False
        elif not self._filter:
            super().handle_endtag(name)

    def handle_charref(self, name):
        if not self._filter:
            super().handle_charref(name)

    def handle_entityref(self, name):
        if not self._filter:
            super().handle_entityref(name)

    def handle_data(self, data):
        if not self._filter:
            super().handle_data(data)

class HTMLToText(HTMLParser):

    HEADER_TAGS = ['h%d' % i for i in range(1, 7)]
    LIST_TAGS = ['ul', 'ol', 'dl']
    LIST_ITEM_TAGS = ['li', 'dd']
    STARTEND_NEWLINE_TAGS = ['br']
    START_NEWLINE_TAGS = ['p', 'br'] + HEADER_TAGS + LIST_TAGS
    END_NEWLINE_TAGS = ['p'] + HEADER_TAGS

    def __init__(self, unwrap_tags=[], extract_tags=DEFAULT_EXTRACT_TAGS):
        self._html_unwrapper = HTMLUnwrapper(filter_tags=unwrap_tags)
        self._html_extractor = HTMLExtractor(filter_tags=extract_tags)
        super().__init__(convert_charrefs=True)

    def reset(self):
        super().reset()
        self._buf = []
        self._html_unwrapper.reset()
        self._html_extractor.reset()

    def feed(self, data):
        self._html_unwrapper.feed(data)
        self._html_unwrapper.close()
        self._html_extractor.feed(self._html_unwrapper.get_data())
        self._html_extractor.close()
        super().feed(self._html_extractor.get_data())

    def close(self):
        self._html_unwrapper.close()
        self._html_extractor.close()
        super().close()

    def handle_startendtag(self, name, attrs):
        if name in self.STARTEND_NEWLINE_TAGS:
            self._buf.append('\n')

    def handle_starttag(self, name, attrs):
        if name in self.START_NEWLINE_TAGS:
            self._buf.append('\n')

    def handle_endtag(self, name):
        if name in self.END_NEWLINE_TAGS:
            self._buf.append('\n')

    def handle_data(self, data):
        if data:
            self._buf.append(data)

    def get_text(self):
        text = re.sub(r'\n\s+\n', '\n\n', ''.join(self._buf))
        return re.sub(r' +', ' ', text).strip()

def _html_to_text_std(html, unwrap_tags=[], extract_tags=DEFAULT_EXTRACT_TAGS):
    parser = HTMLToText(unwrap_tags=unwrap_tags, extract_tags=extract_tags)
    try:
        parser.feed(html)
        parser.close()
    except:
        pass
    return parser.get_text()

def html_to_text(html, parser='beautifulsoup', unwrap_tags=[], extract_tags=DEFAULT_EXTRACT_TAGS):
    if HAVE_BEAUTIFULSOUP and parser == 'beautifulsoup':
        return _html_to_text_beautifulsoup(html,
                                           unwrap_tags=unwrap_tags,
                                           extract_tags=extract_tags)
    else:
        return _html_to_text_std(html,
                                 unwrap_tags=unwrap_tags,
                                 extract_tags=extract_tags)

def _create_parser():
    parser = ArgumentParser(description="""Converts HTML to its plain text data.""")
    parser.add_argument('-p', '--parser',
                        choices=['beautifulsoup', 'std'],
                        default='beautifulsoup',
                        help="HTML parser used to extract text")
    parser.add_argument('-e', '--extract-tag',
                        action='append',
                        default=DEFAULT_EXTRACT_TAG_NAMES,
                        help="Tags to extract from the HTML")
    parser.add_argument('html_file',
                        help="HTML file for input")
    parser.add_argument('text_file',
                        help="Text file for output")
    parser.add_argument('-V', '--verbose',
                        action='store_true',
                        default=False,
                        help="Enable verbose output")
    return parser

def parse_args():
    return _create_parser().parse_args()

def _read_content(fname):
    with open(fname, 'rb') as f:
        return f.read().decode('utf-8')

def _save_content(fname, content):
    with open(fname, 'wb') as f:
        f.write(content.encode('utf-8'))

def _print_request(args):
    if HAVE_BEAUTIFULSOUP and args.parser == 'beautifulsoup':
        parser = "BeautifulSoup"
    else:
        parser = "HTMLParser"
    print("Converting html file '%s' to text file '%s' using parser %s" %
          (args.html_file, args.text_file, parser))
    print("Extracting tags: %s" % "'" + '\', \''.join(args.extract_tag) + "'")

def main():
    args = parse_args()
    if args.verbose:
        _print_request(args)
    html = _read_content(args.html_file)
    text = html_to_text(html, parser=args.parser,
                        extract_tags=[simple_tag(extract_tag) for extract_tag in args.extract_tag])
    _save_content(args.text_file, text)

if __name__ == '__main__':
    main()
