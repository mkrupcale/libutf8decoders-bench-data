# libutf8decoders-bench-data

## Introduction

`libutf8decoders-bench-data` is a repository storing the benchmark data for `libutf8decoders`[1].

## Installation

To install, simply clone this repo:

```shell
git clone https://gitlab.com/mkrupcale/libutf8decoders-bench-data.git
cd libutf8decoders-bench-data
```

### Requirements

By itself this repository has no requirements, but running the script to fetch the data requires:
 - `wget`
 - `python3`
 - `python3-mwclient`[2]
 - `python3-beautifulsoup4`[3] (optional)

## Usage

To fetch the benchmark data, use the provided shell script:

```shell
./scripts/fetch-benchmark-data.sh all
```

## License

The contents of this repository are licensed under the MIT license, except where otherwise noted. Wikipedia data, in particular, is licensed under the Creative Commons Attribution Share-Alike 3.0 Unsupported license.

## References

1. [`libutf8decoders`](https://gitlab.com/mkrupcale/libutf8decoders.git)
2. [`python3-mwclient`](https://mwclient.readthedocs.io/en/latest/)
3. [`python3-beautifulsoup4`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
